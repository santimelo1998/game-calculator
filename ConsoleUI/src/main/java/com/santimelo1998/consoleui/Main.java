/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.santimelo1998.consoleui;

import com.santimelo1998.dataaccess.entities.Data;
import com.santimelo1998.dataaccess.entities.Game;
import com.santimelo1998.dataaccess.entities.Player;
import com.santimelo1998.gcbusinesslogic.controllers.BLController;
import com.santimelo1998.gcbusinesslogic.exceptions.NotFoundException;
import com.santimelo1998.gcbusinesslogic.exceptions.PreExistingException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBException;

/**
 *
 * @author Usuario
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            BLController blc = new BLController();
            try {
                blc.removeGame("Half-Life 2");
//                blc.save();
            } catch (NotFoundException ex) {
                System.err.println("Juego no existe");
            }
        } catch (JAXBException ex) {
            System.out.println("Error al cargar o guardar el xml");
        }
    }

}
