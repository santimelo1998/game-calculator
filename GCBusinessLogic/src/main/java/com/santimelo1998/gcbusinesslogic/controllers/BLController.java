/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.santimelo1998.gcbusinesslogic.controllers;

import com.santimelo1998.dataaccess.controllers.Controller;
import com.santimelo1998.dataaccess.entities.Data;
import com.santimelo1998.dataaccess.entities.Game;
import com.santimelo1998.dataaccess.entities.Player;
import com.santimelo1998.gcbusinesslogic.exceptions.NotFoundException;
import com.santimelo1998.gcbusinesslogic.exceptions.PreExistingException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.xml.bind.JAXBException;

/**
 *
 * @author Usuario
 */
public class BLController {

    private Data data;

    public BLController() throws JAXBException {
        try {
            this.data = Controller.LoadData();
        } catch (JAXBException ex) {
            Controller.SaveData(new Data(new ArrayList<>(), new ArrayList<>()));
            this.data = Controller.LoadData();
        }
    }

    private void save() throws JAXBException {
        Controller.SaveData(this.data);
    }

    public List<Game> getGames() {
        return this.data.getGames();
    }

    public Game getGame(String name) throws NotFoundException {
        for (Game game : this.data.getGames()) {
            if (game.getName().compareTo(name) == 0) {
                return game;
            }
        }
        throw new NotFoundException();
    }

    public void gameExists(String name) throws PreExistingException {
        for (Game game : this.data.getGames()) {
            if (game.getName().compareTo(name) == 0) {
                throw new PreExistingException();
            }
        }
    }

    public List<Player> getPlayers() {
        return this.data.getPlayers();
    }

    public Player getPlayer(String name) throws NotFoundException {
        for (Player player : this.data.getPlayers()) {
            if (player.getName().compareTo(name) == 0) {
                return player;
            }
        }
        throw new NotFoundException();
    }

    public void playerExists(String name) throws PreExistingException {
        for (Player player : this.data.getPlayers()) {
            if (player.getName().compareTo(name) == 0) {
                throw new PreExistingException();
            }
        }
    }

    public void addGame(Game game) throws PreExistingException, JAXBException {
        for (Game g : this.data.getGames()) {
            if (g.getName().compareTo(game.getName()) == 0) {
                throw new PreExistingException();
            }
        }
        this.data.getGames().add(game);
        save();
    }

    public void addPlayer(Player player) throws PreExistingException, JAXBException {
        for (Player p : this.data.getPlayers()) {
            if (p.getName().compareTo(player.getName()) == 0) {
                throw new PreExistingException();
            }
        }
        this.data.getPlayers().add(player);
        save();
    }

    public void removeGame(String name) throws NotFoundException, JAXBException {
        Game game = this.getGame(name);
        for (Player player : this.data.getPlayers()) {
            for (Game g : player.getOwnedGames()) {
                if (g.getName().compareTo(name) == 0) {
                    player.getOwnedGames().remove(g);
                    break;
                }
            }
        }
        this.data.getGames().remove(game);
        save();
    }

    public void removePlayer(String name) throws NotFoundException, JAXBException {
        Player player = this.getPlayer(name);
        this.data.getPlayers().remove(player);
        save();
    }

    public void addGameToPlayer(String playerName, String gameName) throws NotFoundException, PreExistingException, JAXBException {
        Player player = this.getPlayer(playerName);
        Game game = this.getGame(gameName);
        if (this.playerHasGame(playerName, gameName)) {
            throw new PreExistingException();
        } else {
            player.getOwnedGames().add(game);
            save();
        }
    }

    public void removeGameFromPlayer(String playerName, String gameName) throws NotFoundException, JAXBException {
        if (this.playerHasGame(playerName, gameName)) {
            Player player = this.getPlayer(playerName);
            for (Game playerGame : player.getOwnedGames()) {
                if (playerGame.getName().compareTo(gameName) == 0) {
                    player.getOwnedGames().remove(playerGame);
                    save();
                    break;
                }
            }
        }
    }

    public void updateGame(Game originalGameData, Game newGameData) throws JAXBException {
        for (Game savedGame : getGames()) {
            if (savedGame.equals(originalGameData)) {
                savedGame.setName(newGameData.getName());
                savedGame.setMinPlayers(newGameData.getMinPlayers());
                savedGame.setMaxPlayers(newGameData.getMaxPlayers());
                break;
            }
        }
        for (Player player : getPlayers()) {
            if (player.getGame(originalGameData.getName()) != null) {
                Game ownedGame = player.getGame(originalGameData.getName());
                ownedGame.setName(newGameData.getName());
                ownedGame.setMinPlayers(newGameData.getMinPlayers());
                ownedGame.setMaxPlayers(newGameData.getMaxPlayers());
            }
        }
        save();
    }

    public void updatePlayer(Player originalPlayerData, Player newPlayerData) throws JAXBException {
        for (Player savedPlayer : getPlayers()) {
            if (savedPlayer.equals(originalPlayerData)) {
                savedPlayer.setName(newPlayerData.getName());
                savedPlayer.setOwnedGames(newPlayerData.getOwnedGames());
                break;
            }
        }
        save();
    }

    public boolean playerHasGame(String playerName, String gameName) throws NotFoundException {
        for (Player player : data.getPlayers()) {
            if (player.getName().compareTo(playerName) == 0) {
                for (Game playerGame : player.getOwnedGames()) {
                    if (playerGame.getName().compareTo(gameName) == 0) {
                        return true;
                    }
                }
                return false;
            }
        }
        throw new NotFoundException();
    }

    public List<Game> calculateGames(List<String> playersNames) throws NotFoundException {
        List<Game> games;
        List<Player> players = new ArrayList<>();
        for (String playerName : playersNames) {
            players.add(getPlayer(playerName));
        }
        games = sharedGames(players);
        games = validGames(games, players.size());
        return games;
    }

    private List<Game> sharedGames(List<Player> players) {
        List<Game> games = new ArrayList<>();
        boolean firstTime = true;
        for (Player player : players) {
            if (games.isEmpty() && firstTime) {
                games = player.getOwnedGames();
                firstTime = false;
            } else {
                games = games.stream().distinct().filter(player.getOwnedGames()::contains).collect(Collectors.toList());
            }
        }
        return games;
    }

    private List<Game> validGames(List<Game> games, int numberOfPlayers) {
        List<Game> auxGames = new ArrayList(games);
        for (Game auxGame : auxGames) {
            for (Game game : games) {
                if (auxGame.getName().compareTo(game.getName()) == 0) {
                    if (game.getMinPlayers() > numberOfPlayers || game.getMaxPlayers() < numberOfPlayers) {
                        games.remove(game);
                    }
                    break;
                }
            }
        }
        return games;
    }

    public List<Game> filterGames(String name) {
        List<Game> filteredGames = new ArrayList<>();
        for (Game game : data.getGames()) {
            if (game.getName().toLowerCase().matches(".*" + name.toLowerCase() + ".*")) {
                filteredGames.add(game);
            }
        }
        return filteredGames;
    }

}
