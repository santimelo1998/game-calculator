/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.santimelo1998.gcbusinesslogic.exceptions;

/**
 *
 * @author Usuario
 */
public class NotFoundException extends Exception{
    public NotFoundException(){
        super("Object not found");
    }
}
