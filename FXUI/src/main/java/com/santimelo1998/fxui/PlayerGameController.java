/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.santimelo1998.fxui;

import com.santimelo1998.dataaccess.entities.Game;
import com.santimelo1998.gcbusinesslogic.controllers.BLController;
import com.santimelo1998.gcbusinesslogic.exceptions.NotFoundException;
import com.santimelo1998.gcbusinesslogic.exceptions.PreExistingException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import javax.xml.bind.JAXBException;

/**
 * FXML Controller class
 *
 * @author santiago
 */
public class PlayerGameController implements Initializable {

    private BLController controller;
    private String playerName;
    @FXML
    private TitledPane playerGameTitledPane;
    @FXML
    private ListView<Game> gamesListView;
    @FXML
    private ListView<Game> ownedGamesListView;
    @FXML
    private TextField searchTextField;
    @FXML
    private Button addButton;
    @FXML
    private Label playerLabel;
    @FXML
    private Button removeButton;
    @FXML
    private Button cancelButton;
    @FXML
    private Button confirmButton;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        gamesListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        ownedGamesListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }

    /**
     *
     * @param playerName
     */
    public void init(String playerName) throws JAXBException, NotFoundException {
        this.playerName = playerName;
        controller = new BLController();
        playerGameTitledPane.setText("Juegos de " + this.playerName);
        playerLabel.setText("Juegos de " + this.playerName);
        playerLabel.layoutXProperty().set(456);
        playerLabel.layoutYProperty().set(7);
        List<Game> games = new ArrayList(controller.getGames());
        List<Game> ownedGames = controller.getPlayer(this.playerName).getOwnedGames();
        for (Game ownedGame : ownedGames) {
            for (Game game : games) {
                if (ownedGame.getName().compareTo(game.getName()) == 0) {
                    games.remove(game);
                    break;
                }
            }
        }
        gamesListView.setItems(FXCollections.observableArrayList(games));
        ownedGamesListView.setItems(FXCollections.observableArrayList(new ArrayList()));
        updateGameListView();
    }

    @FXML
    private void handleAddButton(ActionEvent event) {
        if (gamesListView.getSelectionModel().getSelectedItems().size() >= 1) {
            for (Game game : gamesListView.getSelectionModel().getSelectedItems()) {
                ownedGamesListView.getItems().add(game);
            }
            updateGameListView();
        }
    }

    private void updateGameListView() {
        for (Game ownedGame : ownedGamesListView.getItems()) {
            for (Game game : gamesListView.getItems()) {
                if (game.getName().compareTo(ownedGame.getName()) == 0) {
                    gamesListView.getItems().remove(game);
                    break;
                }
            }
        }
    }

    @FXML
    private void handleRemoveButton(ActionEvent event) {
        if (ownedGamesListView.getSelectionModel().getSelectedItems().size() >= 1) {
            List<Game> games = new ArrayList<>(ownedGamesListView.getSelectionModel().getSelectedItems());
            for (Game game : games) {
                for (Game ownedGame : ownedGamesListView.getItems()) {
                    if (game.getName().compareTo(ownedGame.getName()) == 0) {
                        ownedGamesListView.getItems().remove(ownedGame);
                        break;
                    }
                }
            }
            gamesListView.setItems(FXCollections.observableArrayList(controller.getGames()));
            updateGameListView();
        }
    }

    @FXML
    private void handleCancelButton(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/Scene.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        FXMLController fxmlc = loader.getController();
        fxmlc.init("Players");
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
    }

    @FXML
    private void handleConfirmButton(ActionEvent event) throws JAXBException, IOException {
        for (Game game : ownedGamesListView.getItems()) {
            try {
                controller.addGameToPlayer(playerName, game.getName());
            } catch (NotFoundException ex) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setContentText("Jugador o juego no encontrado.");
                alert.showAndWait();
            } catch (PreExistingException ex) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setContentText(playerName + " ya tiene el juego " + game.getName() + ".");
                alert.showAndWait();
            }
        }
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/Scene.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        FXMLController fxmlc = loader.getController();
        fxmlc.init("Players");
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
    }

    @FXML
    private void changedSearchTextField(KeyEvent event) {
        if (!searchTextField.getText().isEmpty()) {
            gamesListView.setItems(FXCollections.observableArrayList(new ArrayList(controller.filterGames(searchTextField.getText()))));
        }
        else{
            gamesListView.setItems(FXCollections.observableArrayList(new ArrayList(controller.getGames())));
        }
        updateGameListView();
    }
}
