package com.santimelo1998.fxui;

import com.santimelo1998.dataaccess.entities.Game;
import com.santimelo1998.dataaccess.entities.Player;
import com.santimelo1998.gcbusinesslogic.controllers.BLController;
import com.santimelo1998.gcbusinesslogic.exceptions.NotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javax.xml.bind.JAXBException;

public class FXMLController implements Initializable {

    @FXML
    private TableView<Game> gamesTable;
    @FXML
    private TableColumn<Game, String> nameColumn;
    @FXML
    private TableColumn<Game, Integer> minColumn;
    @FXML
    private TableColumn<Game, Integer> maxColumn;
    @FXML
    private Button newGameButon;
    private Game gameSelected;
    @FXML
    private Button editButton;
    @FXML
    private Button deleteButton;
    @FXML
    private ListView<Player> playersListView;
    @FXML
    private Button newPlayerButton;
    @FXML
    private TabPane mainTabPane;
    @FXML
    private Tab gamesTab;
    @FXML
    private Tab playersTab;
    @FXML
    private Button editPlayerButton;
    private Player playerSelected;
    @FXML
    private Button deletePlayerButton;
    @FXML
    private TableView<Game> playerGamesTable;
    @FXML
    private Button newPlayerGameButton;
    @FXML
    private TableColumn<Game, String> namePlayerGameColumn;
    @FXML
    private TableColumn<Game, Integer> minPlayerGameColumn;
    @FXML
    private TableColumn<Game, Integer> maxPlayerGameColumn;
    @FXML
    private Button removePlayerGameButton;
    @FXML
    private ListView<Player> allPlayersListView;
    @FXML
    private ListView<Player> addedPlayerListView;
    private Tab selectedTab;
    @FXML
    private Tab calculatorTab;
    @FXML
    private Button addButton;
    @FXML
    private Button removeButton;
    @FXML
    private TableView<Game> calculatedGamesTable;
    @FXML
    private TableColumn<Game, String> calculatedNameColumn;
    @FXML
    private TableColumn<Game, Integer> calculatedMinColumn;
    @FXML
    private TableColumn<Game, Integer> calculatedMaxColumn;
    BLController controller;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            controller = new BLController();
            selectedTab = calculatorTab;
            allPlayersListView.setItems(FXCollections.observableArrayList(new ArrayList(controller.getPlayers())));
            addedPlayerListView.setItems(FXCollections.observableArrayList(new ArrayList()));
            calculatedNameColumn.setCellValueFactory(new PropertyValueFactory<Game, String>("name"));
            calculatedMinColumn.setCellValueFactory(new PropertyValueFactory<Game, Integer>("minPlayers"));
            calculatedMaxColumn.setCellValueFactory(new PropertyValueFactory<Game, Integer>("maxPlayers"));
            ObservableList<Game> games = FXCollections.observableArrayList(controller.getGames());
            ObservableList<Player> players = FXCollections.observableArrayList(controller.getPlayers());
            disableGameButtons();
            disablePlayerButtons();
            removePlayerGameButton.setDisable(true);
            nameColumn.setCellValueFactory(new PropertyValueFactory<Game, String>("name"));
            minColumn.setCellValueFactory(new PropertyValueFactory<Game, Integer>("minPlayers"));
            maxColumn.setCellValueFactory(new PropertyValueFactory<Game, Integer>("maxPlayers"));
            gamesTable.setItems(games);
            namePlayerGameColumn.setCellValueFactory(new PropertyValueFactory<Game, String>("name"));
            minPlayerGameColumn.setCellValueFactory(new PropertyValueFactory<Game, Integer>("minPlayers"));
            maxPlayerGameColumn.setCellValueFactory(new PropertyValueFactory<Game, Integer>("maxPlayers"));
            playersListView.setItems(players);
            playerGamesTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
            allPlayersListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
            addedPlayerListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        } catch (JAXBException ex) {
            System.err.println("Error");
        }
    }

    /**
     *
     * @param tab
     */
    public void init(String tab) {
        switch (tab) {
            case "Calculator":
                mainTabPane.getSelectionModel().select(calculatorTab);
            case "Games":
                mainTabPane.getSelectionModel().select(gamesTab);
                break;
            case "Players":
                mainTabPane.getSelectionModel().select(playersTab);
        }
    }

    @FXML
    private void handleNewGameButton(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/NewGame.fxml"));
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Styles.css");
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
    }

    @FXML
    private void rowSelected(MouseEvent event) {
        if (gamesTable.getSelectionModel().getSelectedItems().size() == 1) {
            gameSelected = gamesTable.getSelectionModel().getSelectedItem();
            enableGameButtons();
        }
    }

    @FXML
    private void handleEditButton(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/NewGame.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        NewGameController newGameController = loader.getController();
        newGameController.init(gameSelected);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
    }

    @FXML
    private void handleDeleteButton(ActionEvent event) throws JAXBException {
        try {
            controller.removeGame(gameSelected.getName());
            ObservableList<Game> games = FXCollections.observableArrayList(controller.getGames());
            gamesTable.setItems(games);
            gameSelected = null;
            disableGameButtons();
        } catch (NotFoundException ex) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setContentText("Juego no encontrado.");
            alert.showAndWait();
        }
    }

    @FXML
    private void handleNewPlayerButton(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/Player.fxml"));
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Styles.css");
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
    }

    @FXML
    private void handleEditPlayerButton(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/Player.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        PlayerController playerController = loader.getController();
        playerController.init(playerSelected);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
    }

    @FXML
    private void handleClickPlayerListView(MouseEvent event) throws JAXBException {
        if (playersListView.getSelectionModel().getSelectedItems().size() == 1) {
            playerSelected = playersListView.getSelectionModel().getSelectedItem();
            playerGamesTable.setItems(FXCollections.observableArrayList(playerSelected.getOwnedGames()));
            enablePlayerButtons();
        }
    }

    @FXML
    private void handleDeletePlayerButton(ActionEvent event) throws JAXBException {
        try {
            controller.removePlayer(playerSelected.getName());
            playersListView.setItems(FXCollections.observableArrayList(controller.getPlayers()));
            playerSelected = null;
            disablePlayerButtons();
        } catch (NotFoundException ex) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setContentText("Jugador no encontrado.");
            alert.showAndWait();
        }
    }

    private void enableGameButtons() {
        editButton.setDisable(false);
        deleteButton.setDisable(false);
    }

    private void disableGameButtons() {
        editButton.setDisable(true);
        deleteButton.setDisable(true);
    }

    private void enablePlayerButtons() {
        editPlayerButton.setDisable(false);
        deletePlayerButton.setDisable(false);
        newPlayerGameButton.setDisable(false);
    }

    private void disablePlayerButtons() {
        editPlayerButton.setDisable(true);
        deletePlayerButton.setDisable(true);
        newPlayerGameButton.setDisable(true);
    }

    @FXML
    private void handleNewPlayerGameButton(ActionEvent event) throws JAXBException, IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/PlayerGame.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        PlayerGameController playerGameController = loader.getController();
        try {
            playerGameController.init(playerSelected.getName());
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            stage.setScene(scene);
        } catch (NotFoundException ex) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setContentText("Jugador no encontrado.");
            alert.showAndWait();
        }
    }

    @FXML
    private void handleRemovePlayerGameButton(ActionEvent event) throws JAXBException {
        try {
            for (Game game : playerGamesTable.getSelectionModel().getSelectedItems()) {
                controller.removeGameFromPlayer(playerSelected.getName(), game.getName());
            }
            playersListView.setItems(FXCollections.observableArrayList(controller.getPlayers()));
            for (Player player : playersListView.getItems()) {
                if (player.getName().compareTo(playerSelected.getName()) == 0) {
                    playerSelected = player;
                    playersListView.getSelectionModel().select(player);
                }
            }
            playerGamesTable.setItems(FXCollections.observableArrayList(controller.getPlayer(playerSelected.getName()).getOwnedGames()));
        } catch (NotFoundException ex) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setContentText("Jugador no encontrado.");
            alert.showAndWait();
        }
    }

    @FXML
    private void clickPlayerGameTable(MouseEvent event) {
        if (playerGamesTable.getSelectionModel().getSelectedItems().size() >= 1) {
            removePlayerGameButton.setDisable(false);
        }
    }

    @FXML
    private void clickMainTabPane(MouseEvent event) throws JAXBException {
        if (mainTabPane.getSelectionModel().getSelectedItem() != selectedTab) {
            selectedTab = mainTabPane.getSelectionModel().getSelectedItem();
            if (selectedTab == calculatorTab) {
                updatePlayers();
            }
        }
    }

    private void updatePlayers() throws JAXBException {
        allPlayersListView.setItems(FXCollections.observableArrayList(new ArrayList(controller.getPlayers())));
        for (Player addedPlayer : addedPlayerListView.getItems()) {
            for (Player player : allPlayersListView.getItems()) {
                if (addedPlayer.getName().compareTo(player.getName()) == 0) {
                    allPlayersListView.getItems().remove(player);
                    break;
                }
            }
        }
    }

    @FXML
    private void handleAddButton(ActionEvent event) throws JAXBException {
        for (Player player : allPlayersListView.getSelectionModel().getSelectedItems()) {
            addedPlayerListView.getItems().add(player);
        }
        updatePlayers();
        updateCalculatedGamesTable();
    }

    @FXML
    private void handleRemoveButton(ActionEvent event) throws JAXBException {
        List<Player> addedPlayers = new ArrayList(addedPlayerListView.getSelectionModel().getSelectedItems());
        for (Player player : addedPlayers) {
            for (Player addedPlayer : addedPlayerListView.getItems()) {
                if (addedPlayer.getName().compareTo(player.getName()) == 0) {
                    addedPlayerListView.getItems().remove(addedPlayer);
                    break;
                }
            }
        }
        updatePlayers();
        updateCalculatedGamesTable();
    }

    private void updateCalculatedGamesTable() throws JAXBException {
        List<String> playerNames = new ArrayList<>();
        for (Player player : addedPlayerListView.getItems()) {
            playerNames.add(player.getName());
        }
        try {
            calculatedGamesTable.setItems(FXCollections.observableArrayList(controller.calculateGames(playerNames)));
        } catch (NotFoundException ex) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setContentText("Jugador no encontrado.");
            alert.showAndWait();
        }
    }

}
