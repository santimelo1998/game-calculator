/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.santimelo1998.fxui;

import com.santimelo1998.dataaccess.entities.Game;
import com.santimelo1998.dataaccess.entities.Player;
import com.santimelo1998.gcbusinesslogic.controllers.BLController;
import com.santimelo1998.gcbusinesslogic.exceptions.PreExistingException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javax.xml.bind.JAXBException;

/**
 * FXML Controller class
 *
 * @author santiago
 */
public class PlayerController implements Initializable {

    @FXML
    private Button cancelButton;
    @FXML
    private Button confirmButton;
    @FXML
    private TextField nameTextField;
    private Player player;
    BLController controller;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            controller = new BLController();
            player = null;
        } catch (JAXBException ex) {
            Logger.getLogger(PlayerController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param selectedPlayer
     */
    public void init(Player selectedPlayer) {
        player = selectedPlayer;
        nameTextField.setText(player.getName());
    }

    @FXML
    private void handleCancelButton(ActionEvent event) throws IOException {
        returnMain(event);
    }

    @FXML
    private void handleConfirmButton(ActionEvent event) throws JAXBException, IOException {
        if (!nameTextField.getText().isEmpty()) {
            try {
                if (player == null) {
                    player = new Player(nameTextField.getText(), new ArrayList<Game>());
                    controller.addPlayer(player);
                } else {
                    if (player.getName().compareTo(nameTextField.getText()) != 0) {
                        controller.playerExists(nameTextField.getText());
                    }
                    Player original = new Player(player);
                    player.setName(nameTextField.getText());
                    controller.updatePlayer(original, player);
                }
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("/fxml/Scene.fxml"));
                Parent root = loader.load();
                Scene scene = new Scene(root);
                FXMLController fXMLController = loader.getController();
                fXMLController.init("Players");
                Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                stage.setScene(scene);
            } catch (Exception ex) {
                if (ex.getClass() == PreExistingException.class) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setContentText("El jugador ya existe.");
                    alert.showAndWait();
                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setContentText("Jugador no encontrado.");
                    alert.showAndWait();
                }
            }
        }
    }

    private void returnMain(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/Scene.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        FXMLController controller = loader.getController();
        controller.init("Players");
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
    }

}
