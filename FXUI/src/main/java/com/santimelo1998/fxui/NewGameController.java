/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.santimelo1998.fxui;

import com.santimelo1998.dataaccess.entities.Game;
import com.santimelo1998.gcbusinesslogic.controllers.BLController;
import com.santimelo1998.gcbusinesslogic.exceptions.PreExistingException;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javax.xml.bind.JAXBException;

/**
 * FXML Controller class
 *
 * @author santiago
 */
public class NewGameController implements Initializable {

    @FXML
    private Button confirmButton;
    @FXML
    private Button cancelButton;
    @FXML
    private TextField nameTextField;
    @FXML
    private TextField minPlayersTextField;
    @FXML
    private TextField maxPlayersTextField;
    private Game game;
    BLController controller;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            controller = new BLController();
            game = null;
        } catch (JAXBException ex) {
            Logger.getLogger(NewGameController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param selectedGame
     */
    public void init(Game selectedGame) {
        game = selectedGame;
        nameTextField.setText(game.getName());
        minPlayersTextField.setText(String.valueOf(game.getMinPlayers()));
        maxPlayersTextField.setText(String.valueOf(game.getMaxPlayers()));
    }

    @FXML
    private void handleConfirmButton(ActionEvent event) throws JAXBException, IOException {
        if (!nameTextField.getText().isEmpty() && !minPlayersTextField.getText().isEmpty() && !maxPlayersTextField.getText().isEmpty()) {
            if (minPlayersTextField.getText().matches("^[0-9]*$") && maxPlayersTextField.getText().matches("^[0-9]*$")) {
                try {
                    if (game == null) {
                        game = new Game(nameTextField.getText(), Integer.parseInt(minPlayersTextField.getText()), Integer.parseInt(maxPlayersTextField.getText()));
                        controller.addGame(game);
                    } else {
                        if (game.getName().compareTo(nameTextField.getText()) != 0) {
                            controller.gameExists(nameTextField.getText());
                        }
                        Game original = new Game(game);
                        game.setName(nameTextField.getText());
                        game.setMinPlayers(Integer.parseInt(minPlayersTextField.getText()));
                        game.setMaxPlayers(Integer.parseInt(maxPlayersTextField.getText()));
                        controller.updateGame(original, game);
                    }
                    FXMLLoader loader = new FXMLLoader();
                    loader.setLocation(getClass().getResource("/fxml/Scene.fxml"));
                    Parent root = loader.load();
                    Scene scene = new Scene(root);
                    FXMLController fxmlc = loader.getController();
                    fxmlc.init("Games");
                    Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                    stage.setScene(scene);
                } catch (Exception ex) {
                    if (ex.getClass() == PreExistingException.class) {
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("Error");
                        alert.setContentText("El juego ya existe.");
                        alert.showAndWait();
                    } else {
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("Error");
                        alert.setContentText("Juego no encontrado.");
                        alert.showAndWait();
                    }

                }

            } else {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Advertencia");
                alert.setContentText("Máximo y mínimo tiene que ser numérico.");
                alert.showAndWait();
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Advertencia");
            alert.setContentText("Por favor complete todos los campos.");
            alert.showAndWait();
        }
    }

    @FXML
    private void handleCancelButton(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/Scene.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        FXMLController fxmlc = loader.getController();
        fxmlc.init("Games");
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
    }

}
