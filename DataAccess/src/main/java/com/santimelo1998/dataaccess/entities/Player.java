/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.santimelo1998.dataaccess.entities;

import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

/**
 *
 * @author Usuario
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Player {

    @XmlAttribute
    private String name;
    @XmlElementWrapper(name = "games")
    @XmlElement(name = "game")
    private List<Game> ownedGames;

    public Player() {
    }

    public Player(String name, List<Game> ownedGames) {
        this.name = name;
        this.ownedGames = ownedGames;
    }

    public Player(Player player) {
        this.name = player.getName();
        this.ownedGames = player.getOwnedGames();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Game> getOwnedGames() {
        return ownedGames;
    }

    public void setOwnedGames(List<Game> ownedGames) {
        this.ownedGames = ownedGames;
    }

    public Game getGame(String gameName) {
        for (Game game : ownedGames) {
            if (game.getName().compareTo(gameName) == 0) {
                return game;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return this.name;
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Player) {
            Player other = (Player) obj;
            return name.compareTo(other.getName()) == 0;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.name);
        hash = 29 * hash + Objects.hashCode(this.ownedGames);
        return hash;
    }

}
