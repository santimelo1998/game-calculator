/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.santimelo1998.dataaccess.controllers;

import com.santimelo1998.dataaccess.entities.Data;
import java.io.File;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author Usuario
 */
public class Controller {

    public Controller() {
    }
    
    public static void SaveData(Data data) throws JAXBException{
        JAXBContext jaxbc = JAXBContext.newInstance(Data.class);
        Marshaller marshaller = jaxbc.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(data, new File(System.getProperty("user.dir") + System.getProperty("file.separator") + "data.xml"));
    }
    
    public static Data LoadData() throws JAXBException{
        JAXBContext jaxbc = JAXBContext.newInstance(Data.class);
        Unmarshaller unmarshaller = jaxbc.createUnmarshaller();
        Data data = (Data) unmarshaller.unmarshal(new File(System.getProperty("user.dir") + System.getProperty("file.separator") + "data.xml"));
        return data;
    }
}
